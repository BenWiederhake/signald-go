#!/bin/bash -e
if [ -z ${CI_COMMIT_REF_NAME+x} ]; then
    # CI_COMMIT_REF_NAME is unset, try to derive it from git
    git rev-parse --abbrev-ref HEAD
else
    echo "${CI_COMMIT_REF_NAME}"
fi
    