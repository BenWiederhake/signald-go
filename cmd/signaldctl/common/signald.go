package common

import (
	"fmt"
	"log"
	"strings"

	"github.com/google/uuid"
	"github.com/jedib0t/go-pretty/v6/table"

	"gitlab.com/signald/signald-go/signald"
	"gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

const (
	OutputFormatDefault  = "default"
	OutputFormatCSV      = "csv"
	OutputFormatTable    = "table"
	OutputFormatJSON     = "json"
	OutputFormatYAML     = "yaml"
	OutputFormatQR       = "qr"
	OutputFormatMarkdown = "md"
	OutputFormatMan      = "man"
	OutputFormatQuiet    = "quiet"

	AnnotationNoSocketConnection = "no-socket"
)

var (
	Signald *signald.Signald

	OutputFormat string
)

type InvalidAddressError struct {
	invalidAddress string
}

func NewInvalidAddress(i string) InvalidAddressError {
	return InvalidAddressError{invalidAddress: i}
}

func (i InvalidAddressError) Error() string {
	return fmt.Sprintf("invalid address: %s", i.invalidAddress)
}

func Must(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func StylizeTable(t table.Writer) {
	t.SetStyle(table.StyleLight)
}

func StringToAddress(address string) (v1.JsonAddress, error) {
	if strings.HasPrefix(address, "+") {
		return v1.JsonAddress{Number: address}, nil
	}

	if _, err := uuid.Parse(address); err == nil {
		return v1.JsonAddress{UUID: address}, nil
	}
	return v1.JsonAddress{}, NewInvalidAddress(address)
}

func StringToAddressOrGroup(identifier string) (*v1.JsonAddress, string, error) {
	// if it starts with a +, assume it's an e164
	if strings.HasPrefix(identifier, "+") {
		return &v1.JsonAddress{Number: identifier}, "", nil
	}

	// if it parses as a UUID, assume it's a UUID
	if _, err := uuid.Parse(identifier); err == nil {
		return &v1.JsonAddress{UUID: identifier}, "", nil
	}

	// if it doesn't start with a + and doesn't parse as a UUID, assume it's a group
	return nil, identifier, nil
}
