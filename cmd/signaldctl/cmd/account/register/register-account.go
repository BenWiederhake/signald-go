// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package register

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/signald/signald-go/cmd/signaldctl/common"
	"gitlab.com/signald/signald-go/signald"
	v1 "gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

var (
	voice   bool
	captcha string
	testing bool

	RegisterAccountCmd = &cobra.Command{
		Use:   "register [phone number]",
		Short: "begin the process of creating a new account",
		PreRun: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				common.Must(cmd.Help())
				log.Fatal("must specify phone number")
			}
		},
		Run: func(_ *cobra.Command, args []string) {
			go common.Signald.Listen(nil)
			req := v1.RegisterRequest{
				Account: args[0],
				Voice:   voice,
				Captcha: captcha,
			}
			if testing {
				req.Server = signald.StagingServerUUID
			}
			resp, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal(err)
			}

			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatDefault:
				fmt.Println("submit verification code with signaldctl account verify ", resp.AccountId, "000000")
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	RegisterAccountCmd.Flags().BoolVarP(&voice, "voice", "V", false, "request verification code be sent via an automated voice call (code is sent via SMS by default)")
	RegisterAccountCmd.Flags().StringVarP(&captcha, "captcha", "c", "", "a captcha token may be required to register, see https://docs.signald.org/articles/captcha/ for how to get one")
	RegisterAccountCmd.Flags().BoolVarP(&testing, "testing", "t", false, "use the Signal testing server")
}
