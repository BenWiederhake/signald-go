// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package account

import (
	"github.com/spf13/cobra"

	"gitlab.com/signald/signald-go/cmd/signaldctl/cmd/account/delete"
	"gitlab.com/signald/signald-go/cmd/signaldctl/cmd/account/link"
	"gitlab.com/signald/signald-go/cmd/signaldctl/cmd/account/list"
	"gitlab.com/signald/signald-go/cmd/signaldctl/cmd/account/register"
	"gitlab.com/signald/signald-go/cmd/signaldctl/cmd/account/remoteconfig"
	"gitlab.com/signald/signald-go/cmd/signaldctl/cmd/account/setprofile"
	"gitlab.com/signald/signald-go/cmd/signaldctl/cmd/account/verify"
)

var AccountCmd = &cobra.Command{
	Use:     "account",
	Aliases: []string{"accounts"},
}

func init() {
	AccountCmd.AddCommand(delete.DeleteAccountCmd)
	AccountCmd.AddCommand(link.LinkAccountCmd)
	AccountCmd.AddCommand(list.ListAccountCmd)
	AccountCmd.AddCommand(register.RegisterAccountCmd)
	AccountCmd.AddCommand(verify.VerifyAccountCmd)
	AccountCmd.AddCommand(setprofile.SetProfileCmd)
	AccountCmd.AddCommand(remoteconfig.RemoteConfigCmd)
}
