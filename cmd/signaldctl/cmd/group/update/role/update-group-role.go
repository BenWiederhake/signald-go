// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package role

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/signald/signald-go/cmd/signaldctl/common"
	"gitlab.com/signald/signald-go/cmd/signaldctl/config"
	v1 "gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

var (
	account            string
	group              string
	address            v1.JsonAddress
	role               string
	UpdateGroupRoleCmd = &cobra.Command{
		Use:   "role <group> <address> <default|admin>",
		Short: "change a group member's role",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) != 3 {
				common.Must(cmd.Help())
				log.Fatal("not enough arguments provided")
			}
			group = args[0]
			var err error
			address, err = common.StringToAddress(args[1])
			if err != nil {
				log.Fatal(err)
			}
			switch strings.ToLower(args[2]) {
			case "default":
				role = "DEFAULT"
			case "admin":
				role = "ADMINISTRATOR"
			default:
				common.Must(cmd.Help())
				log.Fatal("invalid role specified, please choose 'default' or 'admin'")
			}
		},
		Run: func(_ *cobra.Command, _ []string) {
			go common.Signald.Listen(nil)
			if address.UUID == "" {
				req := v1.ResolveAddressRequest{
					Account: account,
					Partial: &address,
				}
				var err error
				address, err = req.Submit(common.Signald)
				if err != nil {
					log.Fatal("Failed to resolve user address: ", err)
				}
				if address.UUID == "" {
					log.Fatal("Failed to resolve user's UUID")
				}
			}

			req := v1.UpdateGroupRequest{
				Account: account,
				GroupID: group,
				UpdateRole: &v1.GroupMember{
					Role: role,
					UUID: address.UUID,
				},
			}

			resp, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal(err, "error communicating with signald")
			}
			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatCSV, common.OutputFormatTable, common.OutputFormatDefault:
				membere164s := make(map[string]string)
				for _, member := range resp.V2.Members {
					if member.Number == "" {
						continue
					}
					membere164s[member.UUID] = member.Number
				}

				t := table.NewWriter()
				t.AppendHeader(table.Row{"Number", "UUID", "Role"})
				for _, member := range resp.V2.MemberDetail {
					t.AppendRow(table.Row{membere164s[member.UUID], member.UUID, member.Role})
				}
				t.SetOutputMirror(os.Stdout)
				common.StylizeTable(t)
				t.Render()

				if len(resp.V2.PendingMembers) > 0 {
					fmt.Println("Pending Members")

					pendinge164s := make(map[string]string)
					for _, member := range resp.V2.PendingMembers {
						if member.Number == "" {
							continue
						}
						pendinge164s[member.UUID] = member.Number
					}

					t := table.NewWriter()
					t.AppendHeader(table.Row{"Number", "UUID", "Role"})
					for _, member := range resp.V2.PendingMemberDetail {
						t.AppendRow(table.Row{pendinge164s[member.UUID], member.UUID, member.Role})
					}
					t.SetOutputMirror(os.Stdout)
					common.StylizeTable(t)
					t.Render()
				}
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func init() {
	UpdateGroupRoleCmd.Flags().StringVarP(&account, "account", "a", "", "the signald account to use")
}
