package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/signald/signald-go/cmd/signaldctl/common"
	"gitlab.com/signald/signald-go/signald"
)

var protocolCmd = &cobra.Command{
	Use:   "protocol",
	Short: "return a machine-readable description of the signald client protocol. Details at https://signald.org/articles/socket-protocol/",
	RunE: func(cmd *cobra.Command, args []string) error {
		go common.Signald.Listen(nil)

		req := map[string]string{"version": "v1", "type": "protocol", "id": signald.GenerateID()}
		err := common.Signald.RawRequest(req)
		if err != nil {
			log.Println("signald-go: error submitting request to signald")
			return err
		}

		responseChannel := common.Signald.GetResponseListener(req["id"])
		defer common.Signald.CloseResponseListener(req["id"])

		rawResponse := <-responseChannel
		if rawResponse.Error != nil {
			return fmt.Errorf("signald error: %s", string(rawResponse.Error))
		}

		_, err = os.Stdout.Write(rawResponse.Data)
		return err
	},
}

func init() {
	RootCmd.AddCommand(protocolCmd)
}
